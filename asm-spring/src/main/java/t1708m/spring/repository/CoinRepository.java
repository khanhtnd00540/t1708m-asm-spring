package t1708m.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import t1708m.spring.entity.Coin;

import java.util.List;

public interface CoinRepository extends JpaRepository<Coin, Long> {
    List<Coin> findAllByNameAndStatus(String name, int status);
}
