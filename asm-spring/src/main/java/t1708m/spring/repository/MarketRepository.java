package t1708m.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import t1708m.spring.entity.Market;

import java.util.List;

public interface MarketRepository extends JpaRepository<Market, Long> {
    List<Market> findAllByNameAndStatus(String name, int status);
}
