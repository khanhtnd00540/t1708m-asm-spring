package t1708m.spring.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Calendar;

@Entity
public class Market {

    @Id
    private long id;
    private String name;
    private String description;

    private long createdAt;
    private long updatedAd;

    private int status;

    public Market() {
        this.createdAt = Calendar.getInstance().getTimeInMillis();
        this.updatedAd = Calendar.getInstance().getTimeInMillis();
        this.status = 1;
    }

    public Market(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAd() {
        return updatedAd;
    }

    public void setUpdatedAd(long updatedAd) {
        this.updatedAd = updatedAd;
    }

    public long getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
