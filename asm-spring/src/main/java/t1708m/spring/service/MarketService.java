package t1708m.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import t1708m.spring.entity.Market;
import t1708m.spring.repository.MarketRepository;

import java.util.Calendar;
import java.util.List;
@Service
public class MarketService {

    @Autowired
    MarketRepository marketRepository;

    public List<Market> search(String name) {
        return marketRepository.findAllByNameAndStatus(name, 1);
    }

    public Market create(Market market){
        market.setId(Calendar.getInstance().getTimeInMillis());
        marketRepository.save(market);
        return market;
    }

    public List<Market> getListMarket(){
        return marketRepository.findAll();
    }

    public Market findById(long id){
        return marketRepository.findById(id).orElse(null);
    }

}
