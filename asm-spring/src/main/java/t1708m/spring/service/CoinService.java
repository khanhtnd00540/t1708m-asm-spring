package t1708m.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import t1708m.spring.entity.Coin;
import t1708m.spring.repository.CoinRepository;

import java.util.Calendar;
import java.util.List;

@Service
public class CoinService {
    @Autowired
    CoinRepository coinRepository;

    public List<Coin> search(String name) {
        return coinRepository.findAllByNameAndStatus(name, 1);
    }

    public Coin create(Coin coin){
        coin.setId(Calendar.getInstance().getTimeInMillis());
        coinRepository.save(coin);
        return coin;
    }

    public List<Coin> getListCoin(){
        return coinRepository.findAll();
    }

    public Coin findById(long id){
        return coinRepository.findById(id).orElse(null);
    }
}
