package t1708m.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import t1708m.spring.entity.Coin;
import t1708m.spring.service.CoinService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/coin")
public class CoinController {
    @Autowired
    CoinService coinService;

    @RequestMapping(method = RequestMethod.GET, value = "/searchCoin")
    public ResponseEntity<List<Coin>> search(@RequestParam String name) {
        List<Coin> coinList = coinService.search(name);
        return new ResponseEntity<>(coinList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Coin>> list() {
        List<Coin> coinList = coinService.getListCoin();
        return new ResponseEntity<>(coinList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Coin> store(@RequestBody Coin obj){
        try {
            Coin createCoin = coinService.create(obj);
            return new ResponseEntity<>(createCoin, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
