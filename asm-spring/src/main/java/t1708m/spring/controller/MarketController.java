package t1708m.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import t1708m.spring.entity.Market;
import t1708m.spring.service.MarketService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/market")
public class MarketController {

    @Autowired
    MarketService marketService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Market>> list() {
        List<Market> marketList = marketService.getListMarket();
        return new ResponseEntity<>(marketList, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Market> store(@RequestBody Market obj){
        try {
            Market createMarket = marketService.create(obj);
            return new ResponseEntity<>(createMarket, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
